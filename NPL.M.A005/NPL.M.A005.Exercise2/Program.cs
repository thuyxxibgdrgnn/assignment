﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A005.Exercise2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập email cần kiểm tra: ");
            string email = Console.ReadLine();
            if (CheckEmail(email))
            {
                Console.WriteLine("Email hợp lệ.");
            }
            else
            {
                Console.WriteLine("Email không hợp lệ.");
            }

        }
        static bool CheckEmail(string email)
        {
            
            string pattern = @"^[a-zA-Z0-9!#$%&'*+\-/=?^_`{|}~]+(\.[a-zA-Z0-9!#$%&'*+\-/=?^_`{|}~]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$";
            return Regex.IsMatch(email, pattern) && !email.StartsWith(".") && !email.EndsWith(".") && !email.StartsWith("-") && !email.EndsWith("-"); 
        }
    }
}
