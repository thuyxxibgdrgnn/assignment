﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A005.Exercise1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            
            Console.Write("Nhập tên cần chuyển đổi: ");
            string name = Console.ReadLine();
            string newName = NormalizeName(name);
            Console.Write("Tên sau chuyển đổi là: "+ newName);
            
            Console.WriteLine();

        }
        static string NormalizeName(string name)
        {
            string newName = "";
            string[] words = name.Split(' ');
            foreach (string item in words)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    string firstWord = char.ToUpper(item[0]) +"";
                    string nextWord = item.Substring(1).ToLower();
                    newName += firstWord  + nextWord + " ";
                    
                }
            }
            newName = newName.Trim();
            return newName;
        }
    }
}
