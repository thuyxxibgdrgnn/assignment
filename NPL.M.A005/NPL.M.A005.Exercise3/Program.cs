﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A005.Exercise3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Số lượng tên mà bạn muốn nhập: ");
            int n =int.Parse(Console.ReadLine());
            string[] nameList = new string[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write($"Nhập tên thứ " +(i+1) +": ");
                nameList[i] = Console.ReadLine();
            }
            SortName(nameList);

            Console.WriteLine("Danh sách sau khi sắp xếp:");
            foreach (string name in nameList)
            {
                Console.WriteLine(name);
            }

        }
        static void SortName(string[] arr)
        {
            Array.Sort(arr, (x, y) =>
            {
                string[] nameX = x.Split(' ');
                string[] nameY = y.Split(' ');

                string lastNameX = nameX.Length == 1 ? nameX[0] : nameX[nameX.Length - 1];
                string lastNameY = nameY.Length == 1 ? nameY[0] : nameY[nameY.Length - 1];

                return lastNameX.CompareTo(lastNameY);
            });
        }
    }
}
