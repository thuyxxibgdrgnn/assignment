﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class Employee
    {
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

       

        public Employee()
        {
        }

        public Employee(string sSN, string firstName, string lastName, DateTime birthDate, string phone, string email)
        {
            SSN = sSN;
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
            Phone = phone;
            Email = email;
        }
        public Employee(string ssn, string firstName, string lastName)
        {
            SSN = ssn;
            FirstName = firstName;
            LastName = lastName;
        }
        public virtual void Display()
        {
            Console.WriteLine($"SSN: {SSN}");
            Console.WriteLine($"Name: {FirstName} {LastName}");
            Console.WriteLine($"Birth Date: {BirthDate:dd/MM/yyyy}");
            Console.WriteLine($"Phone: {Phone}");
            Console.WriteLine($"Email: {Email}");
        }
    }
}
