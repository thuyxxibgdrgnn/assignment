﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class Program
    {
        static void Main(string[] args)
        {


            List<Employee> employees = new List<Employee>();
            Validate validate = new Validate();
            Manager manager = new Manager();
            
            int choice = 0;
            do
            {
                Console.WriteLine("====Assignment 06 - EmployeeManagement =====");
                Console.WriteLine("1. Import Employee");
                Console.WriteLine("2. Display Employee information");
                Console.WriteLine("3. Search Employees");
                Console.WriteLine("4. Exit");
                choice = validate.checkIntLimit("Enter Menu Option Number: ", 1, 4);
                switch (choice)
                {
                    case 1:
                        manager.ImportEmployee();
                        break;
                    case 2:
                        manager.DisplayInformationEmployee();
                        break;
                    case 3:
                        manager.SearchEmployee();
                        break;
                }
            } while (choice != 4);

            
        }
       

    }
}
