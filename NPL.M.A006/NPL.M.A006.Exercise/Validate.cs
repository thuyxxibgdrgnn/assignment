﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class Validate
    {
        public int checkIntLimit(String mess, int a, int b)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());
                    if (input >= a && input <= b)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Must be in " + a + "-" + b);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid option");
                }
            } while (true);
            return input;
        }
        public string CheckPhoneNumber(string message)
        {
         
            string phoneNumber;
            while (true)
            {
                Console.Write(message);
                phoneNumber = Console.ReadLine();

                if (IsPhoneNumberValid(phoneNumber))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Phone Number, it must consist of 7 digits or more! ");
                }
            }
            return phoneNumber;
        }

        public bool IsPhoneNumberValid(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber) || phoneNumber.Length < 7)
            {
                return false;
            }

            foreach (char digit in phoneNumber)
            {
                if (!char.IsDigit(digit))
                {
                    return false;
                }
            }

            return true;
        }
        public string CheckString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                return input;
            }

        }
        public DateTime CheckDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string intput = Console.ReadLine();
                try
                {
                    DateTime date = DateTime.ParseExact(intput, "dd/MM/yyyy", null);
                    return date;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid format date: ");
                }
            }

        }
        


        }
}
