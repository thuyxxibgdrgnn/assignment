﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class Manager
    {
        public List<Employee> employeeList = new List<Employee>();
        Validate validate = new Validate();

        public void ImportEmployee() {
            Console.WriteLine("======== Import Employee ========");
            Console.WriteLine("1. Salaried Employee");
            Console.WriteLine("2. Hourly Employee");
            Console.WriteLine("3. Main Menu");
            int choice = validate.checkIntLimit("Enter Menu Option Number: ", 1, 3);
            string ssn = validate.CheckString("Enter SSN: ", "^[A-Za-z0-9]+$");
            string firstName = validate.CheckString("Enter First Name: ", "^[A-Za-z\\s]+$");
            string lastName = validate.CheckString("Enter Last Name: ", "^[A-Za-z\\s]+$");
            DateTime birthDate = validate.CheckDate("Enter BirthDate: ");
            string phone = validate.CheckPhoneNumber("Enter Phone Number: ");
            string email = validate.CheckString("Enter email: ", "^[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+(\\.[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$");
            switch (choice)
            {
                 case 1:
                        Console.Write("Enter Commission Rate: ");
                        double commissionRate = double.Parse(Console.ReadLine());
                        Console.Write("Enter Gross Sales: ");
                        double grossSales = double.Parse(Console.ReadLine());
                        Console.Write("Enter Basic Salary: ");
                        double basicSalary = double.Parse(Console.ReadLine());
                        SalariedEmployee salariedEmployee = new SalariedEmployee(ssn, firstName, lastName, birthDate, phone, email, commissionRate, grossSales, basicSalary);
                        employeeList.Add(salariedEmployee);
                        break;
                 case 2:
                        Console.Write("Enter Wage: ");
                        double wage = double.Parse(Console.ReadLine());
                        Console.Write("Enter WorkingHour: ");
                        double workingHour = double.Parse(Console.ReadLine());
                        HourlyEmployee hourlyEmployee = new HourlyEmployee(ssn, firstName, lastName, birthDate, phone, email, wage, workingHour);
                        employeeList.Add(hourlyEmployee);
                        break;
                 case 3:
                    break;
            }
            


        }
        public void DisplayInformationEmployee()
        {
            
            int salariedCount = 0;
            int hourlyCount = 0;
            Console.WriteLine("Hourly Employee: ");
            Console.WriteLine(string.Format("{0,-5}  {1,-12} {2,-12} {3,-20} {4,-15} {5,-20} {6,-10} {7,-5} ",
                                             "SSN", "First Name", "Last Name", "Birth Date", "Phone", "Email", "Wage", "Working Hours"));
            foreach (Employee employee in employeeList)
            {
                if (employee is HourlyEmployee)
                {
                    hourlyCount = 1;
                    HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                    Console.WriteLine(hourlyEmployee.ToString());
                }
            }
            if (hourlyCount == 0) Console.WriteLine("Not Found");
            Console.WriteLine();
            Console.WriteLine("Salaried Employee: ");
            Console.WriteLine(string.Format("{0,-5}  {1,-12} {2,-12} {3,-15} {4,-12} {5,-15} {6,-10} {7,-5} {8,-5} ",
                                            "SSN", "First Name", "Last Name", "Birth Date", "Phone", "Email", "Comminssion Rate", "Gross Sales", "Basic Salary"));
            foreach (Employee employee in employeeList)
            {
                if (employee is SalariedEmployee)
                {
                    salariedCount = 1;
                    SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                    Console.WriteLine(salariedEmployee.ToString());
                }
            }
            if (salariedCount == 0) Console.WriteLine("Not Found");
            Console.WriteLine();
        }
        public void SearchEmployee()
        {
            Console.WriteLine("===== Search Employee =====");
            Console.WriteLine("1. By Employee Type");
            Console.WriteLine("2. By Employee Name");
            Console.WriteLine("3. Main Menu");
            int choice = validate.checkIntLimit("Enter Menu Option Number: ", 1, 3);
            if (choice == 1)
            {
                string employeeType = validate.CheckString("Enter Employee Type: ", "^[A-Za-z\\s]+$");
                if (employeeType.Equals("Hourly") || employeeType.Equals("hourly"))
                {
                    Console.WriteLine(string.Format("{0,-5}  {1,-12} {2,-12} {3,-20} {4,-15} {5,-20} {6,-10} {7,-5}  ",
                                              "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
                    foreach (Employee employee in employeeList)
                    {
                        if (employee is HourlyEmployee)
                        {
                            HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                            Console.WriteLine(hourlyEmployee.ToString());
                        }
                    }
                }
                if (employeeType.Equals("Salaried") || employeeType.Equals("salaried"))
                {
                    Console.WriteLine(string.Format("{0,-5}  {1,-12} {2,-12} {3,-15} {4,-12} {5,-15} {6,-10} {7,-5} {8,-5}",
                "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "comminssionRate", "grossSales", "basicSalary"));
                    foreach (Employee employee in employeeList)
                    {
                        if (employee is SalariedEmployee)
                        {
                            SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                            Console.WriteLine(salariedEmployee.ToString());
                        }
                    }
                }
            }
            else if (choice == 2)
            {
                int hourlyEmployeeCount = 0;
                int salariedEmployeeCount = 0;
                string employeeName = validate.CheckString("Enter Name: ", "^[A-Za-z\\s]+$");
                Console.WriteLine("Hourly Employee:");
                Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} ",
                            "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
                foreach (Employee employee in employeeList)
                {
                    if (employee is HourlyEmployee && employee.FirstName.Equals(employeeName) || employee.LastName.Equals(employeeName))
                    {
                        hourlyEmployeeCount = 1;
                        HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                        Console.WriteLine(hourlyEmployee.ToString());
                    }
                }
                if (hourlyEmployeeCount == 0) Console.WriteLine("Not Found");
                Console.WriteLine("Salaried Employee");
                Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} {8,-5} ",
                "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "comminssionRate", "grossSales", "basicSalary"));
                foreach (Employee employee in employeeList)
                {
                    if (employee is SalariedEmployee && employee.FirstName.Equals(employeeName) || employee.LastName.Equals(employeeName))
                    {
                        salariedEmployeeCount = 1;
                        SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                        Console.WriteLine(salariedEmployee.ToString());
                    }
                }
                if (salariedEmployeeCount == 0) Console.WriteLine("Not Found");
            }
        }
        



    }
}
