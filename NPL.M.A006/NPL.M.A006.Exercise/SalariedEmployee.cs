﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class SalariedEmployee : Employee
    {
        private double basicSalary;
        private double comminssionRate;
        private double grossSales;

        public double CommissionRate { get; set; }
        public double GrossSales { get; set; }
        public double BasicSalary { get; set; }

        public SalariedEmployee()
        {
        }

        public SalariedEmployee(double commissionRate, double grossSales, double basicSalary)
        {
            CommissionRate = commissionRate;
            GrossSales = grossSales;
            BasicSalary = basicSalary;
        }
        
        public SalariedEmployee(string ssn, string firstName, string lastName,
            DateTime birthDate, string phone, string email,
            double comminssionRate, double grossSales, double basicSalary) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.basicSalary = basicSalary;
            this.comminssionRate = comminssionRate;
            this.grossSales = grossSales;
        }

        public override string ToString()
        {
            return string.Format("{0,-5}  {1,-12} {2,-12} {3,-15} {4,-12} {5,-15} {6,-10} {7,-5} {8,-5}",
               SSN, FirstName, LastName, BirthDate.ToString("dd/MM/yyy"), Phone, Email, BasicSalary, CommissionRate, GrossSales);

        }



    }
}
