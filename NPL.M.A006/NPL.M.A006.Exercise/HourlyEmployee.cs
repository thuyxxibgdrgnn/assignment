﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class HourlyEmployee : Employee
    {
        public double Wage { get; set; }
        public double WorkingHour { get; set; }

        public HourlyEmployee() { }

        public HourlyEmployee(string ssn, string firstName, string lastName,
            DateTime birthDate, string phone, string email,
            double wage, double workingHour) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.Wage = wage;
            this.WorkingHour = workingHour;
        }


        




        public override string ToString()
        {
            return string.Format("{0,-5}  {1,-12} {2,-12} {3,-20} {4,-15} {5,-20} {6,-10} {7,-5} ",
                SSN, FirstName, LastName, BirthDate.ToString("dd/MM/yyy"),Phone, Email, Wage, WorkingHour);
        }

    }
}
