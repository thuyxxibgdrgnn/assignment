﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A003.Exercise1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            bool isValidDate = false;
            
            while (!isValidDate)
            {
                Console.Write("Enter day (dd/MM/yyyy): ");
                string inputDate = Console.ReadLine();

                if (DateTime.TryParseExact(inputDate, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out DateTime date))
                {
                    CalculateAndPrintReminders(date);
                    isValidDate = true;
                }
                else
                {
                    Console.WriteLine("Invalid format. Please enter base on this format dd/MM/yyyy.");
                }
            }
        }

        static void CalculateAndPrintReminders(DateTime date)
        {
            int count = 0;
            while (count < 5)
            {
                count++;
                if (count == 1)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        date = date.AddDays(1);
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                        {
                            date = date.AddDays(2);
                        }
                        else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                        {
                            date = date.AddDays(1);
                        }
                    }
                    Console.WriteLine("First reminder: " + date.ToString("dd/MM/yyyy"));
                }

                if (count == 2)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        date = date.AddDays(1);
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                        {
                            date = date.AddDays(2);
                        }
                        else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                        {
                            date = date.AddDays(1);
                        }
                    }
                    Console.WriteLine("Second reminder: " + date.ToString("dd/MM/yyyy"));
                }

                if (count > 2)
                {
                    for (int i = 0; i < 1; i++)
                    {
                        date = date.AddDays(1);
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                        {
                            date = date.AddDays(2);
                        }
                        else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                        {
                            date = date.AddDays(1);
                        }
                    }
                    Console.WriteLine(count + "th reminder: " + date.ToString("dd/MM/yyyy"));
                }
            }

        }
    }
}
