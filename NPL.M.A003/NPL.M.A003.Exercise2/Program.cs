﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A003.Exercise2
{
    internal class Program
    {




        public static List<string> InputRecord()
        {
            List<string> records = new List<string>();
            while (true)
            {
                ConsoleKeyInfo press = Console.ReadKey();

                if (press.Key == ConsoleKey.Enter && press.Modifiers == ConsoleModifiers.Control)
                {
                    break;
                }

                string record = press.KeyChar + Console.ReadLine();
                records.Add(record);
            }
            return records;
        }

        public static bool CheckFormatRecord(List<string> records)
        {
            foreach (string record in records)
            {
                if (!IsRecordFormatValid(record))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsRecordFormatValid(string record)
        {
            string[] parts = record.Split(new[] { " at ", " by " }, StringSplitOptions.None);

            if (parts.Length != 3)
            {
                return false;
            }

            if (DateTime.TryParse(parts[1], out _))
            {
                return true;
            }

            return false;
        }

        public static List<DateTime> GetDateTimeList(List<string> records)
        {
            List<DateTime> dates = new List<DateTime>();
            foreach (string record in records)
            {
                string[] parts = record.Split(new[] { " at ", " by " }, StringSplitOptions.None);

                if (DateTime.TryParse(parts[1], out DateTime date))
                {
                    dates.Add(date);
                }
            }
            return dates;
        }

        public static List<string> SortedRecord(List<string> records, List<DateTime> dates)
        {
            List<string> sortedRecord = new List<string>();

            foreach (DateTime date in dates.OrderBy(d => d))
            {
                foreach (string record in records)
                {
                    if (record.StartsWith(date.ToString("dd/MM/yyyy hh:mm:ss tt")))
                    {
                        sortedRecord.Add(record);
                    }
                }
            }
            return sortedRecord;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("Input create and update information: ");
            List<string> records = InputRecord();
            bool check = CheckFormatRecord(records);

            if (check)
            {
                List<DateTime> dates = GetDateTimeList(records);

                if (dates.Count > 0)
                {
                    List<string> sortedRecords = SortedRecord(records, dates);

                    Console.WriteLine("Create and update information after sorting time ascending:");
                    foreach (string record in sortedRecords)
                    {
                        Console.WriteLine(record);
                    }
                }
                else
                {
                    Console.WriteLine("No valid date-time found in the records.");
                }
            }
            else
            {
                Console.WriteLine("Invalid Format Record!");
            }
        }


    }

}



