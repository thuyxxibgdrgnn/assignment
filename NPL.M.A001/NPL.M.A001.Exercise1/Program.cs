﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A001.Exercise1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập a,b,c");
            Console.Write("Nhập a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Nhập b: ");
            int b = int.Parse(Console.ReadLine());
            Console.Write("Nhập c: ");
            int c = int.Parse(Console.ReadLine());
            double delta = b * b - 4 * a * c;
            double x1, x2;
            if (a == 0)
            {
                x1= -c/b;
                Console.WriteLine("Nghiệm của phương trình là x = " + x1);

            }
            else
            {
                if (delta < 0)
                {
                    Console.WriteLine("Phương trình vô nghiệm");
                }
                else if (delta == 0)
                {
                    x1 = -b / (2 * a);
                    Console.WriteLine("Nghiệm của phương trình là nghiệm kép x1 = x2 = " + x1);
                }
                else
                {
                    x1 = (-b - Math.Sqrt(delta)) / (-2 * a);
                    x2 = (-b + Math.Sqrt(delta)) / (-2 * a);
                    Console.WriteLine("Nghiệm của phương trình là x1: " + x1 + ", x2: " + x2);
                }

            }
            
        }
    }
}
