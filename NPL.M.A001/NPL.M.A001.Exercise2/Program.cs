﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A001.Exercise2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập số thập phân cần chuyển sang hệ nhị phân: ");
            int n = int.Parse(Console.ReadLine());
            int[] a = new int[10];
            int i;
            for (i = 0; n > 0; i++)
            {
                a[i] = n % 2;
                n = n / 2;
            }
            Console.Write("Số sau khi chuyển sang nhị phân là: ");
            for (i = i - 1; i >= 0; i--)
            {
                Console.Write(a[i]);
            }
            Console.WriteLine();
        }
    }
}
