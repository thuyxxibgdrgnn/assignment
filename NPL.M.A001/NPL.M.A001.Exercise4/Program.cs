﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A001.Exercise4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập số cần kiểm tra: ");
            int n =int.Parse(Console.ReadLine());
            bool isPrime = true;
            if (n < 2)
            {
                isPrime = false;
            }
            else
            {
                for(int i=2; i<n/2; i++)
                {
                    if (n % i == 0)
                    {
                        isPrime=false;
                        break;
                    }
                }
            }
            if (isPrime)
            {
                Console.WriteLine(n + " Là số nguyên tố");
            }
            else { Console.WriteLine(n + " không phải số nguyên tố"); }
        }
    }
}
