﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A001.Exercise3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập n: ");
            int n=int.Parse(Console.ReadLine());
            Console.WriteLine("Dãy "+n+" số Fibonacci: ");
            Fibonacci(n);
        }
        static void Fibonacci(int n)
        {
            int f0 = 0, f1 = 1;

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(f0);
                int fn = f0 + f1;
                f0 = f1;
                f1 = fn;
            }
        }
    }
}
