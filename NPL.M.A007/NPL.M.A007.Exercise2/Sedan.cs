﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan : Car
    {
        public int length {  get; set; }

        public Sedan(int length)
        {
            this.length = length;
        }

        public Sedan(decimal speed, double regularPrice, string color, int length) : base(speed, regularPrice, color)
        {
        }

        public override double GetSalePrice()
        {
            double dis = 0;
            if (length > 20)
            {
                dis = regularPrice * 0.95;
            }
            else
            {
                dis = regularPrice * 0.9;
            }
            return dis;
        }
        public void DisplayPrice()
        {
            Console.WriteLine(color + " Sedan Price: " + GetSalePrice());
        }
    }
}
