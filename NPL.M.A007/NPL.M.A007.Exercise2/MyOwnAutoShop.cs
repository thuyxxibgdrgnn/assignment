﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan = new Sedan(1000,110,"red", 1000);
        Ford ford = new Ford(2000, 110, "yellow", 2020, 2000);
        Ford ford1 = new Ford(1212, 100, "blue", 2021, 2000);
        Truck truck = new Truck(1500, 110, "red",1234);
        Truck truck1 = new Truck(1002, 110, "pink", 4321);
        public void DisplayPriceofCars()
        {
            sedan.DisplayPrice();
            ford.DisplayPrice();
            ford1.DisplayPrice();
            truck.DisplayPrice();
            truck1.DisplayPrice();
        }

    }
}
