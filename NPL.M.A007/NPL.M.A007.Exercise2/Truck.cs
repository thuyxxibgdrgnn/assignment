﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck: Car
    {
       
        public int weight { get; set; }

        public Truck(decimal speed, double regularPrice, string color, int weight) : base(speed, regularPrice, color)
        {
        }

        public override double GetSalePrice()
        {
            double price = 0;
            if (weight > 2000)
            {
                price = regularPrice * 0.9;
            }
            else
            {
                price = regularPrice * 0.8;
            }
            return price;
        }
        public void DisplayPrice()
        {
            Console.WriteLine(color + " Truck Price: " + GetSalePrice());
        }
    }
}
