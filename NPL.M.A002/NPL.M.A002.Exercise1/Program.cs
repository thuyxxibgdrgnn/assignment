﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A002.Exercise1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            int[] a = new int[20];
            Console.Write("Nhập số phần tử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write("Nhập giá trị a[" + i + "]: ");
                a[i] = int.Parse(Console.ReadLine());
            }
            int imax = int.MinValue;
            for (int i = 0; i < n; i++)
            {
                if (imax < a[i])
                {
                    imax = a[i];
                }

            }
            Console.WriteLine("Giá trị lớn nhất của mảng là: " + imax);
            int imin = int.MaxValue;
            for (int i = 0; i < n; i++)
            {
                if (imin > a[i])
                {
                    imin = a[i];
                }

            }
            Console.WriteLine("Giá trị nhỏ nhất của mảng là: " + imin);

        }
    }
}
