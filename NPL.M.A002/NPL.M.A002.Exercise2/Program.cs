﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A002.Exercise2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập số thứ nhất: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Nhập số thứ hai: ");
            int b = int.Parse(Console.ReadLine());
            int gcd = FindGCD(a, b);
            Console.WriteLine("Ước số chung lớn nhất của "+a+" và "+b+" là "+gcd);

        }
        static int FindGCD(int a, int b){
            while (b != 0) {
                int temp = b;
                b = a % b;
                a= temp;
            }
            return a;
            }
    }
}
