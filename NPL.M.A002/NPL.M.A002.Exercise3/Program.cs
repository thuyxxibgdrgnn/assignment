﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace NPL.M.A002.Exercise3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            int[] a = new int[20];
            Console.Write("Nhập số phần tử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write("Nhập giá trị a[" + i + "]: ");
                a[i] = int.Parse(Console.ReadLine());
            }
            int imax = int.MinValue;
            for (int i = 0; i < n; i++)
            {
                if (imax < a[i])
                {
                    imax = a[i];
                }

            }
            int imin = int.MaxValue;
            for (int i = 0; i < n; i++)
            {
                if (imin > a[i])
                {
                    imin = a[i];
                }

            }
            int gcd = FindGCD(imin, imax);
            Console.WriteLine("Giá trị min và max của mảng là: "+imin+", "+ imax);
            Console.WriteLine("Ước số chung lớn nhất của giá trị min và max của mảng là: "+ gcd);

        }
        static int FindGCD(int a, int b)
        {
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
    }
}
