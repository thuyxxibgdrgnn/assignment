﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A004.Exercise
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Nhập chuỗi ngày tháng: ");
            string input = Console.ReadLine();

            DateTime date;
            if (DateTime.TryParseExact(input, new string[] { "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", "M/d/yyyy h:mm", "M/d/yyyy h:mm" }, null, System.Globalization.DateTimeStyles.None, out date))
            {
                Console.WriteLine("Ngày nhập hợp lệ: " + date.ToString("dd/MM/yyyy hh:mm:ss tt"));
                DateTime lastDayOfMonth = GetLastDayOfMonth(date);
                Console.WriteLine("Ngày cuối cùng của tháng: " + lastDayOfMonth.ToString("dd/MM/yyyy"));
                int workingDays = CountWorkingDays(date);
                Console.WriteLine("Số ngày làm việc trong tháng: " + workingDays);
            }
            else
            {
                Console.WriteLine("Ngày nhập không hợp lệ.");
            }
        }

        static DateTime GetLastDayOfMonth(DateTime date)
        {
            int year = date.Year;
            int month = date.Month;
            return new DateTime(year, month, DateTime.DaysInMonth(year, month));
        }

        static int CountWorkingDays(DateTime date)
        {
            int workingDays = 0;
            int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            for (int day = 1; day <= daysInMonth; day++)
            {
                DateTime currentDate = new DateTime(date.Year, date.Month, day);
                if (currentDate.DayOfWeek >= DayOfWeek.Monday && currentDate.DayOfWeek <= DayOfWeek.Friday)
                {
                    workingDays++;
                }
            }
            return workingDays;
        }
    }
}
