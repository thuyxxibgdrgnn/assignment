﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A008.Exercise;



Console.WriteLine("Optional Arguments: ");
Student student1 = new Student("Thuy", "KS05", "Female", DateTime.Now, 20, "Thach That");
Student student2 = new Student("Tu", "KS05", "Female", DateTime.Now, 20, "Ha Noi");
Student student3 = new Student("Phuc", "KS05", "Male", DateTime.Now, 20, "Ninh Binh");


Console.WriteLine("Named Arguments: ");
Student student4 = new Student(name: "An", @class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 22, address: "Quang Binh", mark: 7, grade: "B", relationship: "Single");
Student student5 = new Student(name: "Lan", @class: "KS05", gender: "Female", entryDate: DateTime.Now, age: 21, address: "Hai Phong", mark: 9, grade: "A", relationship: "Single");
Student student6 = new Student(name: "Nam", @class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 20, address: "Hung Yen", mark: 7, grade: "B", relationship: "Single");


Console.WriteLine("ToString method transmission parameter,: ");
Console.WriteLine(string.Format("{0,-15}  {1,-10} {2,-10} {3,-15} {4,-5} {5,-5}  ",
                            "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Student student = new Student("Thanh Thuy", "KS05", "Female", new DateTime(2002, 3, 19), 21, "123 Main St", 85.5M, "A", "Single");
Console.WriteLine(student.ToString(student.Name, student.Class, student.Gender, student.Relationship, student.Age, student.Grade));

Console.WriteLine("ToString method no parameter transmission: ");
Console.WriteLine(string.Format("{0,-15}  {1,-10} {2,-10} {3,-15} {4,-5} {5,-5}  ",
                            "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student1.ToString());


Console.WriteLine("Graduate method transmission parameter:");
Console.WriteLine(student4.Graduate(3M));

Console.WriteLine("Graduate method no parameter transmission:");
Console.WriteLine(student1.Graduate());
Console.WriteLine();



