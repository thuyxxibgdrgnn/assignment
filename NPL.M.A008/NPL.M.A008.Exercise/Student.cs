﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise
{
    internal class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateTime EntryDate { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal Mark { get; set; }
        public string Grade { get; set; }

        public Student()
        {
        }

        public Student(string name, string @class, string gender, DateTime entryDate, int age, string address, decimal mark = 0, string grade = "F", string relationship = "Single")
        {
            Name = name;
            Class = @class;
            Gender = gender;
            Relationship = relationship;
            EntryDate = entryDate;
            Age = age;
            Address = address;
            Mark = mark;
            Grade = grade;
        }
        public void Graduate(decimal gradePoint = 0)
        {
            if (gradePoint >= 4.0M)
            {
                Grade = "A";
            }
            else if (gradePoint >= 3.7M)
            {
                Grade = "A-";
            }
            else if (gradePoint >= 3.3M)
            {
                Grade = "B+";
            }
            else if (gradePoint >= 3.0M)
            {
                Grade = "B";
            }
            else if (gradePoint >= 2.7M)
            {
                Grade = "B-";
            }
            else if (gradePoint >= 2.3M)
            {
                Grade = "C+";
            }
            else if (gradePoint >= 2.0M)
            {
                Grade = "C";
            }
            else if (gradePoint >= 1.0M)
            {
                Grade = "D";
            }
            else
            {
                Grade = "F (Fail)";
            }
        }
        public string ToString(string name, string @class, string gender, string relationship, int age, string grade)
        {
            return string.Format("{0,-15}  {1,-10} {2,-10} {3,-15} {4,-5} {5,-5} "  ,
                                  Name, Class, Gender, Relationship,Age,Grade);



            
        }

    }
}
